<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link href="/assets/style.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>


<header class="d-flex py-3 header-double">
  <div class="container">
    <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3">
      <ul class="nav col-12 col-md-auto mb-2 justify-content-start mb-md-0">
        <li><a href="#" class="nav-link px-2 link-dark"><i class="fa-brands fa-facebook"></i></a></li>
        <li><a href="#" class="nav-link px-2 link-dark"><i class="fa-brands fa-line"></i></a></li>
        <li><a href="#" class="nav-link px-2 link-dark"><i class="fa-brands fa-instagram"></i></a></li>
        <li><a href="#" class="nav-link px-2 link-dark"><i class="fa-brands fa-youtube"></i></a></li>
        <li><a href="#" class="nav-link px-2 link-dark"><i class="fa-brands fa-twitter"></i></a></li>
      </ul>
      <a href="/" class="d-flex align-items-center justify-content-center text-dark text-decoration-none logo-double">
        D O U B L E
      </a>
      <div class="col-md-3">
        <div class="d-flex">
          <!-- Search form -->
          <form class="input-group w-auto my-auto d-none d-sm-flex">
            <input autocomplete="off" type="search" class="form-control rounded search-header" placeholder="Search" style="min-width: 125px;">
          </form>
          <a href="login.php" class="link-dark icon-header" style="display:inline;"><i class="fa fa-user"></i></a>
          <a href="#" class="link-dark icon-header"><i class="fa-solid fa-basket-shopping"></i></a>
        </div>
      </div>
    </header>
    <ul class="nav col-12 col-md-auto mb-2 justify-content-start mb-md-0">
      <li><a href="/" class="nav-link px-2 link-dark">Home</a></li>
      <li><a href="#" class="nav-link px-2 link-dark">Features</a></li>
      <li><a href="#" class="nav-link px-2 link-dark">Pricing</a></li>
      <li><a href="#" class="nav-link px-2 link-dark">FAQs</a></li>
      <li><a href="#" class="nav-link px-2 link-dark">About</a></li>
    </ul>
  </div>
</header>