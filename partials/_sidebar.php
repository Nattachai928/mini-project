<style>
    .nav-sidebar > .nav-item > .nav-treeview {

        border-radius: .35rem;
        padding: 5px 5px 5px 5px;
        font-size: 14px;
        white-space: nowrap;
        margin: .1rem 0;
    }

    .nav-item > .nav-treeview {
        border-radius: .25rem;
        padding: 5px 5px 5px 15px;
        font-size: 12px !important;
    }

    .sidebar-dark-primary .nav-sidebar > .nav-item > .nav-link.active,
    .sidebar-light-primary .nav-sidebar > .nav-item > .nav-link.active {
        background-color: #ff9235cc;
        color: #fff;
    }

    .nav-treeview > .nav-item > .nav-link.active,
    [class*=sidebar-dark-] .nav-treeview > .nav-item > .nav-link.active:focus,
    [class*=sidebar-dark-] .nav-treeview > .nav-item > .nav-link.active:hover {
        background-color: #ff9235cc !important;
        color: #fff !important;
    }

    .nav-sidebar .menu-open > .nav-link i.right {
        -webkit-transform: rotate(-90deg);
        transform: rotate(90deg) !important;
    }
</style>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index.php" class="brand-link">
        <span class="brand-text font-weight-light text-center">D O U B L E</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2" style="font-size: 14px;">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">จัดการ User</li>
                <li class="nav-item">
                    <a href="home.php" class="nav-link
          <?php echo basename($_SERVER['PHP_SELF']) == 'home.php' ? 'active' : '' ?>">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            หน้าแรก
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fas fa-user"></i>
                        พนักงาน
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="add-employee.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่ม พนักงาน
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-department.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่ม แผนก
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fas fa-id-card"></i>
                        สมาชิก
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 1
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 2
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 3
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 4
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fas fa-id-card"></i>
                        จัดการสิทธิ์
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="setting-department-permission.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    แก้ไขสิทธิ์การเข้าถึง
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-permission.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มสิทธิ์การเข้าถึง
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-page.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มหน้าเพจ
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="view-permission.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    ดูรายละเอียดสิทธิ์การเข้าถึง
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>

            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">สินค้า</li>
                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fas fa-box"></i>
                        จัดการสินค้า
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="add-product-list.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มรายการสินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-product.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มชื่อสินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-type.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มประเภทสินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-color.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มสีสินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-size.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มไซซ์สินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-brand.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มแบรนด์สินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-product-model.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มแบบสินค้า
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-seasonal.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มฤดูกาล
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fas fa-box-open"></i>
                        Stocks
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 1
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 2
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 3
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 4
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">รายละเอียด</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa-regular fa-paper-plane"></i>
                        โปรโมชั่น
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="add-membership.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่ม membership
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="add-promotion.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่ม promotion
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="view-promotion.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    แก้ไขโปรโมชั่น
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa-solid fa-basket-shopping"></i>
                        ยอดการสั่งซื้อ
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 1
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 2
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 3
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    รอดรีมใส่ 4
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-header">การสั่งซื้อสินค้า</li>
                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fa fa-area-chart"></i>
                        ใบเสนอราคา
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="view-quotation.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    เพิ่มใบเสนอราคา
                                </p>
                            </a>
                        </li>
                    </ul>
                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="nav-icon fa fa-area-chart"></i>
                        ใบสั่งซื้อ
                        <i class="right fas fa-angle-right"></i>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="view-purchase.php" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>
                                    ดูใบสั่งซื้อ
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>

                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-header">ผู้จัดการ</li>
                    <li class="nav-item">
                        <a href="" class="nav-link"><i class="nav-icon fa fa-area-chart"></i>
                            ใบเสนอราคา
                            <i class="right fas fa-angle-right"></i>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="manager-view-quotation.php" class="nav-link">
                                    <i class="nav-icon far fa-circle"></i>
                                    <p>
                                        ดูใบเสนอราคา
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav nav-pills nav-sidebar flex-column" role="menu">
                    <li class="nav-header">Reports</li>
                    <li class="nav-item">
                        <a href="view-product-lot.php" class="nav-link">
                            <i class="nav-icon fa-solid fa-chart-column"></i>
                            <p>
                                ล็อตสินค้าทั้งหมด
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="view-report-orderamount.php" class="nav-link">
                            <i class="nav-icon fa-regular fa-thumbs-up"></i>
                            <p>
                                รายงานยอดการสั่งซื้อ
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="view-report-unorderedproducts.php" class="nav-link">
                            <i class="nav-icon fa-regular fa-thumbs-down"></i>
                            <p>
                                รายงานสินค้าที่ไม่ได้สั่งเข้ามา
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" id="logout" class="nav-link">
                            <i class="nav-icon fas fa-power-off"></i>
                            <p>
                                ออกจากระบบ
                            </p>
                        </a>
                    </li>
                </ul>
        </nav>

    </div>
</aside>

<script>
	document.getElementById('logout').addEventListener('click', () => {
		axios.post('api/api-login.php', {
			query: 'logout',
		})
		.then(function (response) {
			const res = response.data;
			if (res.status === true) {
				Swal.fire({
					icon: 'success',
					title: 'สำเร็จ',
					text: `ออกจากระบบสำเร็จ`,
					confirmButtonText: 'ตกลง',
				}).then((result) => {
					window.location = "index.php";
				})
				return;
			}
			// resetFlow();
			return;
		})
		.catch(function (error) {
			console.log(error);
		});
	})
</script>