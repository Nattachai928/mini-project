<?php
ini_set('display_errors', 1);
include_once("../controllers/controllerUser.php");
$controller = new ControllerUser();
$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);
// echo $controller->getData();
// if ($data['query'] == 'register') {
//       if (strlen($data['customer_card_id']) != 13) {
//         $answer = array(
//           "status" => false,
//           "reason" => 'wrong_id_card'
//         );
//         echo json_encode($answer, true);
//         exit();
//       }
if (preg_match("/^[a-zA-z0-9]*$/", $data['customer_password'])) {
    if (preg_match("/^[a-zA-z0-9]*$/", $data['customer_username'])) {
        $validate_customer = $controller->getCustomer("WHERE username = '" . $data['customer_username'] . "'");
        $res = oci_fetch_object($validate_customer);
        $res_num = oci_num_rows($validate_customer);
        // var_dump($res_num);
        // var_dump($data['customer_username']);
        //   $validate_customer_id_card = $controller->getCustomer("WHERE id_card = '" . $data['customer_card_id'] . "'");
        //   $res_id_card = oci_fetch_object($validate_customer_id_card);
        //   $res_num_id_card = oci_num_rows($validate_customer_id_card);
        if ($res_num != 0) {
            $answer = array(
                "status" => false,
                "reason" => 'same_account'
            );
            echo json_encode($answer, true);
            exit();
        }

        //   if ($res_num_id_card != 0) {
        //     $answer = array(
        //       "status" => false,
        //       "reason" => 'same_id_card'
        //     );
        //     echo json_encode($answer, true);
        //     exit();
        //   }

        if ($data['customer_password'] != $data['customer_password_confirm']) {
            $answer = array(
                "status" => false,
                "reason" => 'wrong_confirm_password'
            );
            echo json_encode($answer, true);
            exit();
        }
        $query = $controller->register($data['customer_name'], $data['customer_lastname'], $data['customer_address'], $data['customer_username'], $data['customer_password']);
    } else {
        $answer = array(
            "status" => false,
            "reason" => 'validate_word'
        );
        echo json_encode($answer, true);
        exit();
    }
} else {
    $answer = array(
        "status" => false,
        "reason" => 'validate_word'
    );
    echo json_encode($answer, true);
    exit();
}

if ($query) {
    $answer = array(
        "status" => true,
        "reason" => 'success'
    );
    echo json_encode($answer, true);
    exit();
}
$answer = array(
    "status" => false,
    "reason" => 'failed'
);
echo json_encode($answer, true);
exit();
// }
