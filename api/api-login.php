<?php
//ini_set('display_errors', 1);
session_start();
include_once("../controllers/controllerUser.php");
$controller = new ControllerUser();
$request_body = file_get_contents('php://input');
$data = json_decode($request_body, true);
// echo $controller->getData();
if ($data['query'] == 'login') {
  $query = $controller->checkCustomerLogin($data['username'], $data['password']);
  if ($query) {
    $answer = array(
      "status" => true,
      "reason" => 'success'
    );
    echo json_encode($answer, true);
    exit();
  }
  $answer = array(
    "status" => false,
    "reason" => 'failed'
  );
  echo json_encode($answer, true);
  exit();
}

if ($data['query'] == 'logout') {
  $query = $controller->logoutCustomer();
  if ($query) {
    $answer = array(
      "status" => true,
      "reason" => 'success'
    );
    echo json_encode($answer, true);
    exit();
  }
  exit();
}
