<?php
// error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
include_once $_SERVER['DOCUMENT_ROOT'] . "/mini-project/models/modelUser.php";

class ControllerUser
{
    public $model;
    public function __construct()
    {
        $this->model = new ModelUser();
    }

    public function getCustomer($condition = null, $select_condition = null)
    {
        return $this->model->getCustomer($condition, $select_condition);
    }

    public function getSessionCustomer()
    {
        // var_dump($_SESSION['employee']);
        if ($_SESSION['customer']) {
            return false;
        } else {
            return true;
        }
    }

    public function register($customer_name, $customer_lastname, $customer_address, $customer_username, $customer_password)
    {
        $hash = password_hash($customer_password, PASSWORD_BCRYPT);
        // var_dump($hash);
        // if (password_verify($customer_password, $hash)) {
        //     // echo $hash;
        // } else {
        //     /* Invalid */
        // }
        return $this->model->register($customer_name, $customer_lastname, $customer_address, $customer_username, $hash);
    }

    public function checkCustomerLogin($username, $password)
    {
        $res = $this->model->checkCustomerLogin($username, $password);
        if ($res) {
            $query = $this->getCustomer("WHERE username = '$username'");
            $user_detail = oci_fetch_object($query);
            $_SESSION['customer_username'] = $user_detail->USERNAME;
            $_SESSION['customer'] = true;
            return true;
        }
        return false;
    }

    public function logoutCustomer()
    {
        $_SESSION['customer'] = false;
        session_destroy();
        return true;
    }
}
