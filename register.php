<!DOCTYPE html>
<html>

<!-- import header -->
<?php
include_once('views/header.php');
?>

<body class="bg-grey">
    <div class="py-5">
        <div class="form-register bg-white rounded-3">
            <h1 class="text-center">ลงทะเบียนเข้าใช้งาน</h1>
            <form id="register">
                <div class="mb-2">
                    <label class="form-label">ชื่อ</label>
                    <input type="text" class="form-control" id="customer_name" placeholder="ชื่อ">
                </div>
                <div class="mb-2">
                    <label class="form-label">นามสกุล</label>
                    <input type="text" class="form-control" id="customer_lastname" placeholder="นามสกุล">
                </div>
                <div class="mb-2">
                    <label class="form-label">ที่อยู่</label>
                    <textarea class="form-control" rows="5" id="customer_address" placeholder="ที่อยู่"></textarea>
                </div>
                <div class="mb-2">
                    <label class="form-label">ชื่อผู้ใช้</label>
                    <input type="text" class="form-control" id="customer_username" placeholder="ชื่อผู้ใช้">
                </div>
                <div class="mb-2">
                    <label class="form-label">รหัสผ่าน</label>
                    <input type="password" class="form-control" id="customer_password" placeholder="รหัสผ่าน">
                </div>
                <div class="mb-2">
                    <label class="form-label">ยืนยัน รหัสผ่าน</label>
                    <input type="password" class="form-control" id="customer_password_confirm" placeholder="ยืนยัน รหัสผ่าน">
                </div><br>
                <div class="d-grid gap-2">
                    <button type="submit" class="btn btn-dark btn-lg btn-block center">ลงทะเบียน</button>
                </div>
                <div class="mb-2">
                    <p><a href="login.php">กลับ</p>
                </div>

            </form>
        </div>
    </div>

</body>

<script>
    document.getElementById("register").addEventListener('submit', function(e) {
        e.preventDefault(); // Cancel the native event
        Swal.fire({
            title: 'ประกาศ !!',
            text: "ต้องการเพิ่มใช่หรือไม่ !",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน',
        }).then((result) => {
            console.log(result)
            if (result.isConfirmed) {
                axios.post('api/api-register.php', {
                        customer_name: document.getElementById('customer_name').value,
                        customer_lastname: document.getElementById('customer_lastname').value,
                        customer_address: document.getElementById('customer_address').value,
                        customer_username: document.getElementById('customer_username').value,
                        customer_password: document.getElementById('customer_password').value,
                        customer_password_confirm: document.getElementById('customer_password_confirm').value,
                        query: 'register',
                    })
                    .then(function(response) {

                        const res = response.data;

                        if (res.status === true) {
                            Swal.fire("สำเร็จ", `เพิ่มข้อมูลสำเร็จ`, "success");
                            resetFlow();
                            return;
                        }
                        Swal.fire("ล้มเหลว", `${validate(res.reason)}`, "error");
                        resetFlow();
                        return;
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            }
        })
    });

    function validate(reason) {
        switch (reason) {
            case 'same_account':
                return 'Username ซ้ำกัน !!';
            case 'validate_word':
                return 'ให้ใช้ A-Z a-z 0-9 ใน Username และ Password !';
                // case 'wrong_id_card':
                //   return 'เลขบัตรประชาชนไม่ถูกต้อง';
            case 'wrong_confirm_password':
                return 'รหัสผ่านไม่เหมือนกัน';
                // case 'same_id_card':
                //     return 'เลขบัตรประชาชนซ้ำกัน';
        }
    }

    function resetFlow() {
        document.getElementById("register").reset();
    }
</script>

<!-- import footer -->

</html>
<?php

include_once('views/footer.php')

?>