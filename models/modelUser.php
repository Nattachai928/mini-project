<?php
require_once 'dbconnect.php';
class ModelUser
{
    protected $conn;
    public function __construct()
    {
        $this->conn = oci_connect(username, password, host, 'UTF8');
        if (!$this->conn) echo 'Failed to connect to Oracle';
    }

    public function register($customer_name, $customer_lastname, $customer_address, $customer_username, $customer_password)
    {
        $sql = "insert into CUSTOMER (FNAME,LNAME,ADDRESS,USERNAME,PASSWORD) values ('$customer_name','$customer_lastname','$customer_address','$customer_username', '$customer_password')";
        // var_dump($sql);
        $result = oci_parse($this->conn, $sql);
        $query = oci_execute($result); // like query mysql
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function getCustomer($condition = null, $condition_select = null)
    {
        // $sql = "SELECT * FROM customer $condition";
        // var_dump($sql);
        if ($condition_select != null) {
            $result = oci_parse($this->conn, "SELECT $condition_select FROM customer");
            oci_execute($result); // like query mysql
            return $result;
        }
        if ($condition_select != null && $condition != null) {
            $result = oci_parse($this->conn, "SELECT $condition_select FROM customer $condition");
            oci_execute($result); // like query mysql
            return $result;
        }
        if ($condition != null) {
            $result = oci_parse($this->conn, "SELECT * FROM customer $condition");
            oci_execute($result); // like query mysql
            return $result;
        }

        $result = oci_parse($this->conn, "SELECT * FROM customer");
        oci_execute($result); // like query mysql
        return $result;
    }
    public function checkCustomerLogin($username, $password)
    {
        $res_user = $this->getCustomer("WHERE username = '$username'");
        $res = oci_fetch_object($res_user);
        $res_num = oci_num_rows($res_user);
        if ($res_num == 1) {
            if (password_verify($password,  $res->PASSWORD)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
