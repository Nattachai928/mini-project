<!DOCTYPE html>
<html>

<!-- import header -->
<?php
    include_once('views/header.php');
?>

    <body >
        <h1 class="text-center">Member Page</h1>
        
        <div class="mb-3 text-center">
            <button type="submit" class="btn btn-dark btn-lg btn-block center" id="logout">Logout</button>
        </div>

    </body>

    <script>

//   function getMember() {
//     axios.post('api/api-customer.php', {
//         query: 'get-customer',
//       })
//       .then(function(response) {
//         const res = response.data;
//         document.getElementById('fname').innerHTML += `${res.fname}`
//         document.getElementById('Fname').innerHTML += `${res.Fname}`
//         document.getElementById('lname').innerHTML += `${res.lname}`
//         document.getElementById('point').innerHTML += `${res.point}`
//         document.getElementById('membership').innerHTML += `${res.membership}`
//         document.getElementById('address').innerHTML += `${res.address}`
//         document.getElementById('username').innerHTML += `${res.username}`
//         return;
//       })
//       .catch(function(error) {
//         console.log(error);
//       });
//   }

  document.getElementById('logout').addEventListener('click', (e) => {
    e.preventDefault();
    axios.post('api/api-login.php', {
        query: 'logout',
      })
      .then(function(response) {
        const res = response.data;
        if (res.status === true) {
          Swal.fire({
            icon: 'success',
            title: 'สำเร็จ',
            text: `ออกจากระบบสำเร็จ`,
            confirmButtonText: 'ตกลง',
          }).then((result) => {
            window.location = "login.php";
          })
          return;
        }
        // resetFlow();
        return;
      })
      .catch(function(error) {
        console.log(error);
      });
  })
</script>

</html>

<?php
    include_once('views/footer.php')
?>