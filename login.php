<!DOCTYPE html>
<html>
<?php
include_once('views/header.php');
?>

<body class="bg-grey">
  <div class="py-5">
    <div class="form-register bg-white rounded-3">
      <h2 class="text-center">ลงชื่อเข้าใช้</h2>

      <form id="login">
        <div class="mb-2">
          <label>ชื่อผู้ใช้</label>
          <input type="text" class="form-control" id="user" placeholder="ชื่อผู้ใช้">
        </div>
        <div class="mb-2">
          <label>รหัสผ่าน</label>
          <input type="password" class="form-control" id="password" placeholder="รหัสผ่าน">
        </div><br>
        <div class="d-grid gap-2">
          <button type="submit" class="btn btn-dark btn-lg btn-block center">เข้าสู่ระบบ</button>
        </div>
        <div class="mb-2">
          <p class="text-end"><a href="register.php">ลงทะเบียน</a></p>
        </div>

      </form>

    </div>
  </div>
  </div>

  <script>
    document.getElementById('login').addEventListener('submit', (e) => {
      e.preventDefault();
      axios.post('api/api-login.php', {
          username: document.getElementById('user').value,
          password: document.getElementById('password').value,
          query: 'login',
        })
        .then(function(response) {
          const res = response.data;
          if (res.status === true) {
            Swal.fire({
              icon: 'success',
              title: 'สำเร็จ',
              text: `เข้าสู่ระบบสำเร็จ`,
              confirmButtonText: 'ตกลง',
            }).then((result) => {
              window.location = "member.php";
            })
            return;
          }
          Swal.fire("ล้มเหลว", `เข้าสู่ระบบไม่สำเร็จ`, "error");
          // resetFlow();
          return;
        })
        .catch(function(error) {
          console.log(error);
        });
    })

    function resetFlow() {
      document.getElementById("login").reset();
    }
  </script>

</html>

<?php
include_once('views/footer.php')
?>